// P1.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "P1.h"
#include <commctrl.h>
#include <commdlg.h>

#define MAX_LOADSTRING 100
#define ID_TOOLBAR		1000	// ID of the toolbar
#define IMAGE_WIDTH     18
#define IMAGE_HEIGHT    17
#define BUTTON_WIDTH    0
#define BUTTON_HEIGHT   0
#define TOOL_TIP_MAX_LEN   32

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name
WCHAR childTitle[MAX_LOADSTRING];
int num=0;
HWND hwndMDIClient;
HWND  hToolBarWnd;
HWND hFrame;
int checkMenu = ID_DRAW_LINE;
// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    FrameWndProc(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK    DrawWndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);
void doCreate_ToolBar(HWND hWnd);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_P1, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_P1));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(hFrame, hAccelTable, &msg) && !TranslateMDISysAccel(hwndMDIClient, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = FrameWndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_P1));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_P1);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));
	if (!RegisterClassExW(&wcex)) return FALSE;

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = DrawWndProc;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_P1));
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	//wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = NULL;
	wcex.lpszClassName = L"Draw_WND";

	if (!RegisterClassExW(&wcex)) return FALSE;
    //return RegisterClassExW(&wcex);
}

LRESULT CALLBACK MDICloseProc(HWND hChildWnd, LPARAM lParam)
{
	SendMessage(hwndMDIClient, WM_MDIDESTROY, (WPARAM)hChildWnd, 0L);
	return 1;
}


void CheckMenu(HWND hWnd, int checked, int toCheck)
{
	CheckMenuItem(GetMenu(hWnd), checked, MF_UNCHECKED | MF_BYCOMMAND);
	CheckMenuItem(GetMenu(hWnd), toCheck, MF_CHECKED | MF_BYCOMMAND);
	checkMenu = toCheck;
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK FrameWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
	case WM_CREATE: // trong hàm FrameWndProc
	{
		hFrame = hWnd;
		CheckMenuItem(GetMenu(hWnd), ID_DRAW_LINE, MF_CHECKED | MF_BYCOMMAND);		
		CLIENTCREATESTRUCT ccs;
		ccs.hWindowMenu = GetSubMenu(GetMenu(hWnd), 4);
		ccs.idFirstChild = 50001;


		doCreate_ToolBar(hWnd);
		hwndMDIClient = CreateWindow(L"MDICLIENT", (LPCTSTR)NULL,
			WS_CHILD | WS_CLIPCHILDREN | WS_VSCROLL |
			WS_HSCROLL,
			50, 50, 50, 50,
			hWnd,
			(HMENU)NULL,
			hInst,
			(LPVOID)&ccs);
		
		ShowWindow(hwndMDIClient, SW_SHOW);
		
		return DefFrameProc(hWnd, hwndMDIClient, message, wParam, lParam);
	}
	case WM_SIZE:
	{
		doCreate_ToolBar(hWnd);
		UINT w, h;
		w = LOWORD(lParam);
		h = HIWORD(lParam);
		MoveWindow(hwndMDIClient, 0,0, 0, 0, TRUE);
		return DefFrameProc(hWnd, hwndMDIClient, message, wParam, lParam);
	}
	break;
    case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
            // Parse the menu selections:
            switch (wmId)
            {
			case ID_FILE_NEW:
			{
				num++;
				MDICREATESTRUCT mdiCreate;
				wsprintf(childTitle, L"Noname-%d.drw", num);
				mdiCreate.szClass = L"Draw_WND";
				mdiCreate.szTitle = childTitle;
				mdiCreate.hOwner = hInst;
				mdiCreate.x = CW_USEDEFAULT;
				mdiCreate.y = CW_USEDEFAULT;
				mdiCreate.cx = CW_USEDEFAULT;
				mdiCreate.cy = CW_USEDEFAULT;
				
				mdiCreate.style = 0;
				// Tham số “mở”, chuyển đến WM_CREATE
				mdiCreate.lParam = NULL;
				SendMessage(hwndMDIClient, WM_MDICREATE, 0, (LONG)(LPMDICREATESTRUCT)&mdiCreate);
			}break;
			case ID_FILE_OPEN:
				MessageBox(hWnd, L"You clicked at Open", L"Notice", MB_OK);
				break;
			case ID_FILE_SAVE:
				MessageBox(hWnd, L"You clicked at Save", L"Notice", MB_OK);
				break;
			case ID_DRAW_COLOR:
				{
					CHOOSECOLOR cc; // CTDL dùng cho dialog ChooseColor
					COLORREF acrCustClr[16]; // Các màu do user định nghĩa
					DWORD rgbCurrent = RGB(255, 0, 0); // màu được chọn default
													   // Khởi tạo struct
					ZeroMemory(&cc, sizeof(CHOOSECOLOR));
					cc.lStructSize = sizeof(CHOOSECOLOR);
					cc.hwndOwner = hWnd; // handle của window cha
					cc.lpCustColors = (LPDWORD)acrCustClr;
					cc.rgbResult = rgbCurrent; // trả về màu được chọn
					cc.Flags = CC_FULLOPEN | CC_RGBINIT;
					if (ChooseColor(&cc))
					{
						// xử lý màu được chọn, vd. tạo brush
						HBRUSH hbrush;
						hbrush = CreateSolidBrush(cc.rgbResult);
						rgbCurrent = cc.rgbResult;
					}
					else 
					{
						//doSthg here
					}
				}
				break;
			case ID_DRAW_FONT:
			{
				CHOOSEFONT cf; // CTDL dùng cho dialog ChooseFont
				LOGFONT lf; // CTDL font, lưu kết quả font được chọn
				HFONT hfNew, hfOld;
				// Khởi tạo struct
				ZeroMemory(&cf, sizeof(CHOOSEFONT));
				cf.lStructSize = sizeof(CHOOSEFONT);
				cf.hwndOwner = hWnd; // handle của window cha
				cf.lpLogFont = &lf;
				cf.Flags = CF_SCREENFONTS | CF_EFFECTS;
				if (ChooseFont(&cf)) {
					//doSthg
				}
				else
				{
					//doSthg
				}
			}break;
			case ID_DRAW_ELLIPSE:
				CheckMenu(hWnd, checkMenu, ID_DRAW_ELLIPSE);
				break;
			case ID_DRAW_RECTANGLE:
				CheckMenu(hWnd, checkMenu, ID_DRAW_RECTANGLE);
				break;
			case ID_DRAW_LINE:
				CheckMenu(hWnd, checkMenu, ID_DRAW_LINE);
				break;
			case ID_DRAW_TEXT:
				CheckMenu(hWnd, checkMenu, ID_DRAW_TEXT);
				break;
			case ID_DRAW_SELECTOBJECT:
				CheckMenu(hWnd, checkMenu, ID_DRAW_SELECTOBJECT);
				break;
			case ID_WINDOW_TILE:
				SendMessage(hwndMDIClient, WM_MDITILE, 0, 0L);
				break;
			case ID_WINDOW_CASCADE:
				SendMessage(hwndMDIClient, WM_MDICASCADE, 0, 0L);
				break;
			case ID_WINDOW_CLOSEALL:
				EnumChildWindows(hwndMDIClient, (WNDENUMPROC)MDICloseProc, 0L);
				break;
            case IDM_ABOUT:
                DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
                break;
            case IDM_EXIT:
                DestroyWindow(hWnd);
                break;
            default:
				return DefFrameProc(hWnd, hwndMDIClient, message, wParam, lParam);
            }
        }
        break;
    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
            // TODO: Add any drawing code that uses hdc here...
			RECT rc;
			GetClientRect(hWnd, &rc);
			MoveWindow(hwndMDIClient, 0, 25,rc.right, rc.bottom, TRUE);
            EndPaint(hWnd, &ps);
        }
        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
		return DefFrameProc(hWnd, hwndMDIClient, message, wParam, lParam);
    }
	return DefFrameProc(hWnd, hwndMDIClient, message, wParam, lParam);
}

LRESULT CALLBACK DrawWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static HWND hwndClient, hwndFrame;
	switch (message)
	{
	case WM_MDIACTIVATE:
	{
		hwndClient = GetParent(hWnd);
	}break;
	case WM_COMMAND:
	{
		int wmId = LOWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefMDIChildProc(hWnd, message, wParam, lParam);
		}
	}
	break;
	case WM_PAINT:
	{
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(hWnd, &ps);
		// TODO: Add any drawing code that uses hdc here...
		EndPaint(hWnd, &ps);
	}
	break;
	case WM_DESTROY:
		SendMessage(hwndClient, WM_MDIDESTROY, (WPARAM)hWnd, 0);
		//PostQuitMessage(0);
		break;
	default:
		return DefMDIChildProc(hWnd, message, wParam, lParam);
	}
	return DefMDIChildProc(hWnd, message, wParam, lParam);
}
// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}

void doCreate_ToolBar(HWND hWnd)
{
	InitCommonControls();

	TBBUTTON tbButtons[] =
	{
		// Zero-based Bitmap image, ID of command, Button state, Button style, 
		// ...App data, Zero-based string (Button's label)
		{ STD_FILENEW,	ID_FILE_NEW, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ STD_FILEOPEN,	ID_FILE_OPEN, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ STD_FILESAVE,	ID_FILE_SAVE, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ STD_COPY,	ID_DRAW_TEXT, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ STD_PRINTPRE,	ID_DRAW_SELECTOBJECT, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
	};


	// create a toolbar
	hToolBarWnd = CreateToolbarEx(hWnd,
		WS_CHILD| WS_VISIBLE| WS_BORDER | CCS_ADJUSTABLE | TBSTYLE_TOOLTIPS,
		ID_TOOLBAR,
		sizeof(tbButtons) / sizeof(TBBUTTON),
		HINST_COMMCTRL,
		0,
		tbButtons,
		sizeof(tbButtons) / sizeof(TBBUTTON),
		BUTTON_WIDTH,
		BUTTON_HEIGHT,
		IMAGE_WIDTH,
		IMAGE_HEIGHT,
		sizeof(TBBUTTON));

	TBBUTTON tbButtons1[] =
	{
		{ 0, 0,	TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0 },
		
		{ 0, ID_DRAW_LINE,	TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ 1, ID_DRAW_RECTANGLE,	TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ 2, ID_DRAW_ELLIPSE,	TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 }
	};
	TBADDBITMAP	tbBitmap1 = { hInst, IDB_LINE };
	TBADDBITMAP	tbBitmap2 = { hInst, IDB_RECT};
	TBADDBITMAP	tbBitmap3 = { hInst, IDB_ELL };
	int idx = SendMessage(hToolBarWnd, TB_ADDBITMAP, (WPARAM) sizeof(tbBitmap1) / sizeof(TBADDBITMAP),
		(LPARAM)(LPTBADDBITMAP)&tbBitmap1);
	int idx2 = SendMessage(hToolBarWnd, TB_ADDBITMAP, (WPARAM) sizeof(tbBitmap2) / sizeof(TBADDBITMAP),
		(LPARAM)(LPTBADDBITMAP)&tbBitmap2);
	int idx3 = SendMessage(hToolBarWnd, TB_ADDBITMAP, (WPARAM) sizeof(tbBitmap3) / sizeof(TBADDBITMAP),
		(LPARAM)(LPTBADDBITMAP)&tbBitmap3);
	tbButtons1[1].iBitmap = idx;
	tbButtons1[2].iBitmap = idx2;
	tbButtons1[3].iBitmap = idx3;
	SendMessage(hToolBarWnd, TB_ADDBUTTONS, (WPARAM) sizeof(tbButtons1) / sizeof(TBBUTTON),
		(LPARAM)(LPTBBUTTON)&tbButtons1);
}